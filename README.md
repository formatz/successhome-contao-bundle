# Contao bundle to connect Contao instances to SuccessHome API

Install bundle
```bash
composer require formatz/successhome-contao-bundle
```

Migrate database
```bash
vendor/bin/contao-console contao:migrate
```

Configure the bundle in your Contao settings

