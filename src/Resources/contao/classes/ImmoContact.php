<?php

namespace Formatz\SuccesshomeContaoBundle\Resources\contao\classes;

class ImmoContact
{
    private $fistname;
    private $lastname;
    private $email;
    private $phone;
    private $demand;

    public function setDemand($demand)
    {
        $this->demand = $demand;
    }

    public function getDemand()
    {
        return $this->demand;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setFistname($fistname)
    {
        $this->fistname = $fistname;
    }

    public function getFistname()
    {
        return $this->fistname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPhone()
    {
        return $this->phone;
    }
}
