<?php

$GLOBALS['TL_LANG']['FMD']['successhome'] = ['Successhome', ''];
$GLOBALS['TL_LANG']['FMD']['immo_list'] = ['Liste der Immobilien', ''];
$GLOBALS['TL_LANG']['FMD']['immo_reader'] = ['Immobilienleser', ''];
$GLOBALS['TL_LANG']['FMD']['immo_search'] = ['Suchmaschine für Immobilien', ''];
$GLOBALS['TL_LANG']['FMD']['immo_last'] = ['Letzte Immobilien', ''];

$GLOBALS['TL_LANG']['tl_module']['search_fields'] = ['Felder für die Suchmaschine', ''];
$GLOBALS['TL_LANG']['tl_module']['wordingList'] = ['Anzuzeigende Immobilientypen', ''];
$GLOBALS['TL_LANG']['tl_module']['mandatList'] = ['Anzuzeigende Mandatstypen', ''];
$GLOBALS['TL_LANG']['tl_module']['countryList'] = ['Standardland', ''];
$GLOBALS['TL_LANG']['tl_module']['typesList'] = ['Anzuzeigende Immobilientypen', ''];
$GLOBALS['TL_LANG']['tl_module']['inactiveSoldProperties'] = ['Verkaufte Immobilien deaktivieren', 'Verkaufte Immobilien zeigen keinen Preis mehr an und haben keinen Leser mehr'];
$GLOBALS['TL_LANG']['tl_module']['displayEndSoldProperties'] = ['Verkaufte Immobilien am Ende der Liste anzeigen', 'Verkaufte Immobilien werden am Ende der Liste angezeigt'];
