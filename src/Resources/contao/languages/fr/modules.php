<?php

$GLOBALS['TL_LANG']['FMD']['successhome'] = ['Successhome', ''];
$GLOBALS['TL_LANG']['FMD']['immo_list'] = ['Liste des biens immobiliers', ''];
$GLOBALS['TL_LANG']['FMD']['immo_reader'] = ['Lecteur de biens immobiliers', ''];
$GLOBALS['TL_LANG']['FMD']['immo_search'] = ['Moteur de recherche pour biens immobiliers', ''];
$GLOBALS['TL_LANG']['FMD']['immo_last'] = ['Derniers biens immobiliers', ''];

$GLOBALS['TL_LANG']['tl_module']['search_fields'] = ['Champs pour le moteur de recherche', ''];
$GLOBALS['TL_LANG']['tl_module']['wordingList'] = ['Type de biens à afficher', ''];
$GLOBALS['TL_LANG']['tl_module']['mandatList'] = ['Type de mandats à afficher', ''];
$GLOBALS['TL_LANG']['tl_module']['countryList'] = ['Pays par défaut', ''];
$GLOBALS['TL_LANG']['tl_module']['typesList'] = ['Type de biens à afficher', ''];
$GLOBALS['TL_LANG']['tl_module']['inactiveSoldProperties'] = ['Inactiver les biens vendus', 'Les biens vendus n\'affichent plus de prix, et n\'ont plus de reader'];
$GLOBALS['TL_LANG']['tl_module']['displayEndSoldProperties'] = ['Afficher à la fin de liste les biens vendus', 'Les biens vendus sont afficher en fin de liste'];
