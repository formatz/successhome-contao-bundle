<?php

$GLOBALS['TL_LANG']['FMD']['successhome'] = ['Successhome', ''];
$GLOBALS['TL_LANG']['FMD']['immo_list'] = ['Lista de propiedades inmobiliarias', ''];
$GLOBALS['TL_LANG']['FMD']['immo_reader'] = ['Lector de propiedades inmobiliarias', ''];
$GLOBALS['TL_LANG']['FMD']['immo_search'] = ['Motor de búsqueda para propiedades inmobiliarias', ''];
$GLOBALS['TL_LANG']['FMD']['immo_last'] = ['Últimas propiedades inmobiliarias', ''];

$GLOBALS['TL_LANG']['tl_module']['search_fields'] = ['Campos para el motor de búsqueda', ''];
$GLOBALS['TL_LANG']['tl_module']['wordingList'] = ['Tipo de propiedades a mostrar', ''];
$GLOBALS['TL_LANG']['tl_module']['mandatList'] = ['Tipo de mandatos a mostrar', ''];
$GLOBALS['TL_LANG']['tl_module']['countryList'] = ['País por defecto', ''];
$GLOBALS['TL_LANG']['tl_module']['typesList'] = ['Tipo de propiedades a mostrar', ''];
$GLOBALS['TL_LANG']['tl_module']['inactiveSoldProperties'] = ['Inactivar las propiedades vendidas', 'Las propiedades vendidas no muestran precio y no tienen lector'];
$GLOBALS['TL_LANG']['tl_module']['displayEndSoldProperties'] = ['Mostrar al final de la lista las propiedades vendidas', 'Las propiedades vendidas se muestran al final de la lista'];
