<?php

$GLOBALS['TL_LANG']['FMD']['successhome'] = ['Successhome', ''];
$GLOBALS['TL_LANG']['FMD']['immo_list'] = ['List of real estate properties', ''];
$GLOBALS['TL_LANG']['FMD']['immo_reader'] = ['Real estate properties reader', ''];
$GLOBALS['TL_LANG']['FMD']['immo_search'] = ['Search engine for real estate properties', ''];
$GLOBALS['TL_LANG']['FMD']['immo_last'] = ['Latest real estate properties', ''];

$GLOBALS['TL_LANG']['tl_module']['search_fields'] = ['Fields for the search engine', ''];
$GLOBALS['TL_LANG']['tl_module']['wordingList'] = ['Type of properties to display', ''];
$GLOBALS['TL_LANG']['tl_module']['mandatList'] = ['Type of mandates to display', ''];
$GLOBALS['TL_LANG']['tl_module']['countryList'] = ['Default country', ''];
$GLOBALS['TL_LANG']['tl_module']['typesList'] = ['Type of properties to display', ''];
$GLOBALS['TL_LANG']['tl_module']['inactiveSoldProperties'] = ['Deactivate sold properties', 'Sold properties no longer display prices and no longer have a reader'];
$GLOBALS['TL_LANG']['tl_module']['displayEndSoldProperties'] = ['Display sold properties at the end of the list', 'Sold properties are displayed at the end of the list'];
