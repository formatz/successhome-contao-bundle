<?php

namespace Formatz\SuccesshomeContaoBundle\Resources\contao\modules;

use Contao\BackendTemplate;
use Contao\Input;
use Contao\PageModel;
use Contao\Pagination;
use Contao\StringUtil;
use Contao\System;

class ModuleImmoList extends ModuleSuccessHome
{
    protected $strTemplate = 'immolist';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        $request = System::getContainer()->get('request_stack')->getCurrentRequest();

        if ($request && System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest($request)) {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### '.mb_strtoupper($GLOBALS['TL_LANG']['FMD']['immo_list'][0]).' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile()
    {
        $objPage = PageModel::findPublishedById($this->jumpTo);
        $objPagePrestige = $this->jumpToPrestige ? PageModel::findPublishedById($this->jumpToPrestige) : $objPage;
        $objPageBusiness = $this->jumpToBusiness ? PageModel::findPublishedById($this->jumpToBusiness) : $objPage;

        if (null === $objPage) {
            $this->Template->immos = [];

            return 'Please select a reader page';
        }

        // array of options for search
        $options = $this->getImmoFormData();

        $options['city'] = explode(',', $options['city'])[0];

        if ('immo' !== $this->wording) {
            unset($options['types']);
            $options['wording'] = $this->wording;
        }

        if ('both' !== $this->mandat) {
            $options['mandat'] = $this->mandat;
        }

        if ('all' !== $this->types && 'immo' === $this->wording) {
            $options['types'] = $this->types;
        }

        $arrData = $this->getImmoList($options, 1, 0);

        // get count
        $total = count($arrData);
        $this->Template->total = $total;

        // Pagination
        $page = 1;
        if ($this->perPage > 0) {
            $id = 'p';
            $page = Input::get($id) ?: 1;

            // Do not index or cache the page if the page number is outside the range
            if ($page < 1 || $page > max(ceil($total / $this->perPage), 1)) {
                global $objPage;
                $objPage->noSearch = 1;
                $objPage->cache = 0;

                // Send a 404 header
                header('HTTP/1.1 404 Not Found');

                return;
            }

            $objPagination = new Pagination($total, $this->perPage, 7, $id);
            $this->Template->pagination = $objPagination->generate(' ');
        }

        // get list of immo with options, page and limit
        if ('1' === $this->displayEndSoldProperties) {
            $arrData = $this->getImmoList($options);
        } else {
            $arrData = $this->getImmoList($options, $page, $this->perPage);
        }

        usort($arrData, function ($a, $b) {
            if ($a['date_created'] === $b['date_created']) {
                return 0;
            }

            return ($a['date_created'] > $b['date_created']) ? -1 : 1;
        });

        if (!empty($arrData)) {
            if ('1' === $this->displayEndSoldProperties) {
                $items = array_filter($arrData, function ($data1) { return 'sold' !== $data1['wording']; });
                if (count($items) < $page * $this->perPage) {
                    $soldItems = array_filter($arrData, function ($data1) { return 'sold' === $data1['wording']; });
                    $items = array_merge($items, $soldItems);
                }
                $arrData = array_slice($items, ($page - 1) * $this->perPage, $this->perPage);
            }

            // handle link for the reader
            foreach ($arrData as $ref => $data) {
                $slugImmo = explode('/', $data['slug']);
                $param = '/immo/'.end($slugImmo);

                // Contao 5
                if (System::getContainer()->has('contao.routing.content_url_generator')) {
                    $urlGenerator = System::getContainer()->get('contao.routing.content_url_generator');
                    if ('prestige' === $data['wording']) {
                        $link = $urlGenerator->generate($objPagePrestige->current()).$param;
                    } elseif ('yieldImmo' === $data['wording'] || 'commercial' === $data['type']) {
                        $link = $urlGenerator->generate($objPageBusiness->current()).$param;
                    } else {
                        $link = $urlGenerator->generate($objPage->current()).$param;
                    }
                }
                // Contao 4
                else {
                    if ('prestige' === $data['wording']) {
                        $link = $this->generateFrontendUrl($objPagePrestige->row(), $param);
                    } elseif ('yieldImmo' === $data['wording'] || 'commercial' === $data['type']) {
                        $link = $this->generateFrontendUrl($objPageBusiness->row(), $param);
                    } else {
                        $link = $this->generateFrontendUrl($objPage->row(), $param);
                    }
                }

                $arrData[$ref]['href'] = StringUtil::ampersand($link);
            }
        }

        // immos
        $this->Template->immos = count($arrData) ? $arrData : false;
        $this->Template->wording = $this->wording;
        $this->Template->inactiveSoldProperties = $this->inactiveSoldProperties;
        // label for view
        $this->Template->immo_city_label = $GLOBALS['TL_LANG']['MSC']['immo_city_label'];
        $this->Template->immo_state_label = $GLOBALS['TL_LANG']['MSC']['immo_state_label'];
        $this->Template->immo_room_label = $GLOBALS['TL_LANG']['MSC']['immo_room_label'];
        $this->Template->immo_living_space_label = $GLOBALS['TL_LANG']['MSC']['immo_living_space_label'];
        $this->Template->immo_bedroom_label = $GLOBALS['TL_LANG']['MSC']['immo_bedroom_label'];
        $this->Template->read_more_label = $GLOBALS['TL_LANG']['MSC']['read_more'];
        $this->Template->on_demand_label = $GLOBALS['TL_LANG']['MSC']['immo']['priceOnDemand']['_label'];
    }
}
