<?php

namespace Formatz\SuccesshomeContaoBundle\Resources\contao\modules;

use Contao\Environment;
use Contao\Module;
use Contao\System;
use Formatz\SuccesshomeContaoBundle\Resources\contao\classes\ImmoContact;
use Symfony\Component\HttpClient\HttpClient;

class ModuleSuccessHome extends Module
{
    // Routes
    public const ROUTE_GET_ALL_IMMO = '/api/immo/list';
    public const ROUTE_GET_ONE_IMMO = '/api/immo/reader/';
    public const ROUTE_GET_IMMO_TYPES = '/api/immo/types/list';
    public const ROUTE_NEW_CONTACT = '/api/contact/new_contact/';
    public const ROUTE_GET_ALL_COUNTRIES = '/api/immo/country/list';
    public const SEARCH_CITY = '/api/search/geo/';
    public const API_KEY_NAME = 'apikey';
    public const HTTP_RESPONSE_SUCCESS = 200;
    public const ALL_IMMO_INDEX = 'ALL_IMMO_INDEX';

    // Session
    public const SESSION_CONTACT_FIRSTNAME = 'IMMO_CONTACT_FIRSTNAME';
    public const SESSION_CONTACT_LASTNAME = 'IMMO_CONTACT_LASTNAME';
    public const SESSION_CONTACT_EMAIL = 'IMMO_CONTACT_EMAIL';
    public const SESSION_CONTACT_PHONE = 'IMMO_CONTACT_PHONE';
    public const SESSION_CONTACT_DEMAND = 'IMMO_CONTACT_DEMAND';
    public const SESSION_CONTACT_CAPTCHA = 'IMMO_CONTACT_CAPTCHA';
    public const SESSION_SEARCH = 'IMMO_SEARCH';
    public const SESSION_SEARCH_PLACE = 'IMMO_SEARCH_PLACE';
    public const SESSION_SEARCH_RADIUS = 'IMMO_SEARCH_RADIUS';
    public const SESSION_SEARCH_CITY = 'IMMO_SEARCH_CITY';
    public const SESSION_SEARCH_CITY2 = 'IMMO_SEARCH_CITY2';
    public const SESSION_SEARCH_CITY_LAT = 'IMMO_SEARCH_CITY_LAT';
    public const SESSION_SEARCH_CITY_LNG = 'IMMO_SEARCH_CITY_LNG';
    public const SESSION_SEARCH_TYPES = 'IMMO_SEARCH_TYPES';
    public const SESSION_SEARCH_ROOM_TO = 'IMMO_SEARCH_ROOM_TO';
    public const SESSION_SEARCH_ROOM_FROM = 'IMMO_SEARCH_ROOM_FROM';
    public const SESSION_SEARCH_PRICE_TO = 'IMMO_SEARCH_PRICE_TO';
    public const SESSION_SEARCH_PRICE_FROM = 'IMMO_SEARCH_PRICE_FROM';
    public const SESSION_SEARCH_REF = 'IMMO_SEARCH_REF';
    public const SESSION_SEARCH_COUNTRY = 'IMMO_SEARCH_COUNTRY';
    public const SESSION_SEARCH_HOMECREATIONTYPE = 'SESSION_SEARCH_HOMECREATIONTYPE';
    public const SESSION_SEARCH_SEARCH = 'IMMO_SEARCH_SEARCH';
    public const SESSION_DEFAULT_COUNTRY = 'IMMO_DEFAULT_COUNTRY';

    // Default values
    public const DEFAULT_RADIUS = 5;

    protected function compile()
    {
        // TODO: Implement compile() method.
    }

    protected function getImmoFormData()
    {
        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();

        $data = $objSession->get(self::SESSION_SEARCH);

        if (null === $data) {
            $this->resetImmoFormData();
            $data = $objSession->get(self::SESSION_SEARCH);
        }

        // In Contao 5 session is not same as in Contao 4. Force to set default values
        if(!array_key_exists('roomFrom', $data)) {
            $data['roomFrom'] = '';
        }
        if(!array_key_exists('roomTo', $data)) {
            $data['roomTo'] = '';
        }
        if(!array_key_exists('priceFrom', $data)) {
            $data['priceFrom'] = '';
        }
        if(!array_key_exists('priceTo', $data)) {
            $data['priceTo'] = '';
        }

        return $data;
    }

    protected function setImmoFormData($data)
    {
        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();
        $objSession->set(self::SESSION_SEARCH, $data);

        return $this;
    }

    protected function resetImmoFormData()
    {
        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();
        $objSession->set(self::SESSION_SEARCH, [
            'country' => $this->defaultCountry,
            'place' => null,
            'radius' => null,
            'city' => null,
            'cityLat' => null,
            'cityLng' => null,
            'types' => null,
            'room_to' => null,
            'room_from' => null,
            'price_to' => null,
            'price_from' => null,
            'ref' => null,
            'homecreationtype' => null,
        ]);

        return $this;
    }

    protected function saveContactInfoInSession(ImmoContact $contact)
    {
        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();
        $objSession->set(self::SESSION_CONTACT_FIRSTNAME, $contact->getFistname());
        $objSession->set(self::SESSION_CONTACT_LASTNAME, $contact->getLastname());
        $objSession->set(self::SESSION_CONTACT_EMAIL, $contact->getEmail());
        $objSession->set(self::SESSION_CONTACT_PHONE, $contact->getPhone());
        $objSession->set(self::SESSION_CONTACT_DEMAND, $contact->getDemand());
    }

    protected function getImmoList($options = [], $page = 1, $limit = 0)
    {
        $lists = $this->getAllImmo($options, $page, $limit);

        $arrReturn = [];

        if (empty($lists)) {
            return $arrReturn;
        }

        foreach ($lists as $immo) {
            $arrReturn[$immo['reference']] = [
                'slug' => $immo['slug'] ?? '',
                'description' => $immo['description'] ?? '',
                'reference' => $immo['reference'] ?? '',
                'name' => $immo['name'] ?? '',
                'price' => isset($immo['price']) && $immo['price'] > 0 ? $this->formatPrice($immo['price'], $immo['currency']) : $GLOBALS['TL_LANG']['MSC']['immo']['priceOnDemand']['_label'],
                'expense' => isset($immo['expense']) ? $this->formatPrice($immo['expense'], $immo['currency']) : false,
                'room' => $immo['room'] ?? '',
                'living_space' => $immo['living_space'] ?? '',
                'bedroom' => $immo['bedroom'] ?? '',
                'postal' => $immo['postal_code'] ?? '',
                'city' => $immo['city'] ?? '',
                'state' => $immo['state'] ?? '',
                'wording' => $immo['wording'] ?? '',
                'date_created' => $immo['date_created'] ?? '',
                'type' => $immo['type'] ?? '',
                'address' => isset($immo['address_visible']) && $immo['address_visible'] ? $immo['address'] : '',
            ];

            if (count($immo['images'])) {
                $firstImage = array_shift($immo['images']);

                $arrReturn[$immo['reference']]['images']['path'] = $firstImage['path'];

                if (isset($firstImage['path_list'])) {
                    $arrReturn[$immo['reference']]['images']['path_list'] = $firstImage['path_list'];
                }
            }
        }

        return $arrReturn;
    }

    protected function getImmo($slug = '')
    {
        $immo = $this->getImmoBySlug($slug);

        if (empty($immo)) {
            return [];
        }

        $arrReturn = array_merge($immo, [
            'postal' => $immo['postal_code'] ?? '',
            'specs' => [
                'price' => isset($immo['price']) && $immo['price'] > 0 ? $this->formatPrice($immo['price'], $immo['currency']) : $GLOBALS['TL_LANG']['MSC']['immo']['priceOnDemand']['_label'],
                'expense' => isset($immo['expense']) ? $this->formatPrice($immo['expense'], $immo['currency']) : false,
                'availability' => 'date' === $immo['availability'] ? date('d.m.Y', strtotime($immo['availability_date'])) : $GLOBALS['TL_LANG']['MSC']['immo']['availability'][$immo['availability']],
                'rental_gross' => isset($immo['rental_gross']) ? $this->formatPrice($immo['rental_gross'], $immo['currency']) : null,
                'gross_yield' => $immo['gross_yield'] ?? null,
                'room' => $immo['room'] ?? '',
                'living_space' => isset($immo['living_space']) ? $immo['living_space'].' m<sup>2</sup>' : '',
                'commercial_space' => isset($immo['commercial_space']) ? $immo['commercial_space'].' m<sup>2</sup>' : '',
                'bedroom' => $immo['bedroom'] ?? '',
                'bathroom' => $immo['bathroom'] ?? '',
                'wc' => $immo['wc'] ?? '',
                'storey' => $immo['storey'] ?? '',
                'level' => $immo['level'] ?? '',
                'basement' => $immo['basement'] ?? '',
                'byear' => $immo['byear'] ?? '',
                'ryear' => $immo['ryear'] ?? '',
                'volume' => $immo['volume'] ?? '',
                'ground_space' => isset($immo['ground_space']) ? $immo['ground_space'].' m<sup>2</sup>' : '',
                'garden_space' => isset($immo['garden_space']) ? $immo['garden_space'].' m<sup>2</sup>' : '',
                'balanced_space' => $immo['balanced_space'] ?? '',
                'total_area' => isset($immo['total_area']) ? $immo['total_area'].' m<sup>2</sup>' : '',
                'ground_surface_to_build' => isset($immo['ground_surface_to_build']) ? $immo['ground_surface_to_build'].' m<sup>2</sup>' : '',
                'living_surface_to_build' => isset($immo['living_surface_to_build']) ? $immo['living_surface_to_build'].' m<sup>2</sup>' : '',
                'heating' => $immo['heating'] ?? '',
                'heating_type' => $immo['heating_type'] ?? '',
                'type' => $immo['type'] ?? '',
                'type2' => $immo['type2'] ?? '',
                'style' => $immo['style'] ?? '',
                'floor' => $immo['floor'] ?? '',
                'item_state' => $immo['item_state'] ?? '',
                'park_place_int' => $immo['park_place_int'] ?? '',
                'park_place_ext' => $immo['park_place_ext'] ?? '',
                'park_place_int_price' => isset($immo['park_place_int_price']) ? $this->formatPrice($immo['park_place_int_price'], $immo['currency']) : null,
                'park_place_ext_price' => isset($immo['park_place_ext_price']) ? $this->formatPrice($immo['park_place_ext_price'], $immo['currency']) : null,
                'park_place_garage_price' => isset($immo['park_place_garage_price']) ? $this->formatPrice($immo['park_place_garage_price'], $immo['currency']) : null,
            ],
            'owner' => [
                'lastname' => $immo['owner']['lastname'],
                'firstname' => $immo['owner']['firstname'],
                'name' => $immo['owner']['lastname'].' '.$immo['owner']['firstname'],
                'phone' => $immo['owner']['phone'],
                'agency' => $immo['owner']['agency']['name'],
                'image_url' => $immo['owner']['image_url'] ?? null,
            ],
        ]);

        if (!empty($immo['images'])) {
            $arrReturn['images'] = $immo['images'];
        }

        return $arrReturn;
    }

    public function postNewContact($slug, ImmoContact $immoContact)
    {
        $immoContactArr = [];
        $immoContactArr['firstname'] = $immoContact->getFistname();
        $immoContactArr['lastname'] = $immoContact->getLastname();
        $immoContactArr['phone'] = $immoContact->getPhone();
        $immoContactArr['email'] = $immoContact->getEmail();
        $immoContactArr['message'] = $immoContact->getDemand();
        $immoContactArr['ip'] = Environment::get('ip');
        $immoContactArr['referer'] = Environment::get('referer');
        $immoContactArr['user_agent'] = Environment::get('httpUserAgent');

        return $this->postImmoContact($slug, json_encode($immoContactArr));
    }

    protected function getImmoCountries($mandat = '')
    {
        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();
        if (($countries = $objSession->get('WEBSERVICE_COUNTRIES'.$mandat)) === null) {
            $url = self::ROUTE_GET_ALL_COUNTRIES;
            if ('' !== $mandat && 'both' !== $mandat) {
                $url .= '?mandat='.$mandat;
            }

            $allKey = explode('|||', $GLOBALS['TL_CONFIG']['successhome_apikey']);
            $response = [];
            foreach ($allKey as $key) {
                $result = $this->sendRequest($key, $url);
                foreach ($result as $item) {
                    $response[] = $item;
                }
            }

            $countries = [];
            foreach ($response as $index => $country) {
                if ('' !== $country) {
                    $countries[] = strtolower($country);
                }
            }

            $objSession->set('WEBSERVICE_COUNTRIES'.$mandat, $countries);
        }

        return $countries;
    }

    protected function getAllImmo($options = [], $page = 1, $limit = 0)
    {
        $url = self::ROUTE_GET_ALL_IMMO;
        $url .= '?page='.$page;
        $url .= '&limit='.$limit;

        if (isset($options['place'])) {
            $url .= '&city='.$options['place'];
        } elseif (isset($options['city'])) {
            $url .= '&city='.urlencode($options['city']);
        }
        if (isset($options['country'])) {
            $url .= '&country='.$options['country'];
        }
        if (isset($options['cityLng']) && isset($options['cityLat'])) {
            $url .= '&lng='.$options['cityLng'].'&lat='.$options['cityLat'];
        }
        if (isset($options['radius'])) {
            $url .= '&radius='.$options['radius'];
        }
        if (isset($options['mandat'])) {
            $url .= '&mandat='.$options['mandat'];
        }
        if (isset($options['types'])) {
            if (!is_array($options['types'])) {
                $options['types'] = [$options['types']];
            }
            $url .= '&types='.implode(',', $options['types']);
        }
        if (isset($options['roomFrom'])) {
            $url .= '&roomFrom='.$options['roomFrom'];
        }
        if (isset($options['roomTo'])) {
            $url .= '&roomTo='.$options['roomTo'];
        }
        if (isset($options['priceFrom'])) {
            $url .= '&priceFrom='.$options['priceFrom'];
        }
        if (isset($options['priceTo'])) {
            $url .= '&priceTo='.$options['priceTo'];
        }
        if (isset($options['ref'])) {
            $url .= '&ref='.$options['ref'];
        }
        if (isset($options['wording'])) {
            $url .= '&wording='.$options['wording'];
        }
        if (isset($options['search'])) {
            $url .= '&search='.rawurlencode($options['search']);
        }
        if (isset($options['homecreationType'])) {
            $url .= '&homecreationType='.rawurlencode($options['homecreationType']);
        }
        if (isset($options['ignoreSold'])) {
            $url .= '&ignoreSold=1';
        }

        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();
        $allImmo = [];
        $response = [];
        // TODO, find a better way ?
        $allKey = explode('|||', $GLOBALS['TL_CONFIG']['successhome_apikey']);
        foreach ($allKey as $index => $key) {
            $responseOnEnterprise = $this->sendRequest($key, $url);
            if (!is_array($responseOnEnterprise)) {
                continue;
            }
            foreach ($responseOnEnterprise as $rep) {
                $allSlug = explode('/', $rep['slug']);
                $slug = end($allSlug);
                $allImmo[$slug] = $index;
            }
            foreach ($responseOnEnterprise as $rep) {
                $response[] = $rep;
            }
        }
        $objSession->set(self::ALL_IMMO_INDEX, $allImmo);

        return $response;
    }

    protected function getImmoBySlug($slug)
    {
        $url = self::ROUTE_GET_ONE_IMMO.$slug;

        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();
        $key = $this->getKeyForIndex($objSession->get(self::ALL_IMMO_INDEX)[$slug]);
        $immo = $this->sendRequest($key, $url);

        if (empty($immo)) {
            $immos = $this->sendRequestForAllKey($url);
            $immo = array_shift($immos);
        }

        return $immo;
    }

    protected function postImmoContact($slug, $immoContact)
    {
        $url = self::ROUTE_NEW_CONTACT.$slug;

        $objSession = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession();
        $key = $this->getKeyForIndex($objSession->get(self::ALL_IMMO_INDEX)[$slug]);

        $response = $this->sendRequest($key, $url, $immoContact, 'POST');
        if (array_key_exists('status', $response) && self::HTTP_RESPONSE_SUCCESS === $response['status']) {
            return true;
        }

        return false;
    }

    /**
     * Retourne la liste des types disponibles et leur quantité.
     */
    protected function getAvailableTypes()
    {
        static $result;
        if (null === $result) {
            $result = $this->sendRequestForAllKey(self::ROUTE_GET_IMMO_TYPES);
            $result = array_unique($result);

            $key = 'flat';
            if (in_array($key, $result, true)) {
                // mettre appartement en 2ème
                $result = array_merge([$key], array_diff($result, [$key]));
            }
            $key = 'house';
            if (in_array($key, $result, true)) {
                // mettre maison en premier
                $result = array_merge([$key], array_diff($result, [$key]));
            }
        }

        return $result;
    }

    private function sendRequest($key, $path, $data = null, $method = 'GET')
    {
        $client = HttpClient::create();
        $url = $this->getUrl();
        $finalUrl = $url.$path;
        $options = [
            'headers' => [
                self::API_KEY_NAME => $key,
                'Accept' => 'application/json',
            ],
        ];

        if ('POST' === $method) {
            $options['body'] = $data;
        }

        $response = $client->request($method, $finalUrl, $options);
        $statusCode = $response->getStatusCode();

        if (self::HTTP_RESPONSE_SUCCESS !== $statusCode) {
            // Handle error responses here
            // For example, log the error and return an empty array
            error_log(sprintf('Error with request "%s". Status code: %s', $path, $statusCode));

            return [];
        }

        $responseData = $response->toArray();

        if ('POST' === $method) {
            $responseData['status'] = $statusCode;
        }

        return $responseData;
    }

    private function sendRequestForAllKey($url)
    {
        $allKey = explode('|||', $GLOBALS['TL_CONFIG']['successhome_apikey']);
        $results = [];
        foreach ($allKey as $index => $key) {
            $result = $this->sendRequest($key, $url);
            if (null !== $result) {
                $results += $result;
            }
        }

        return $results;
    }

    private function getUrl()
    {
        return $GLOBALS['TL_CONFIG']['successhome_url'];
    }

    private function getApikey()
    {
        return $GLOBALS['TL_CONFIG']['successhome_apikey'];
    }

    private function getKeyForIndex($index)
    {
        $allKey = explode('|||', $GLOBALS['TL_CONFIG']['successhome_apikey']);
        if ($index && isset($allKey[$index]) && count($allKey) > 1) {
            return $allKey[$index];
        }

        return $allKey[0];
    }

    private function formatPrice($fltPrice, $currency = null)
    {
        // If price or override price is a string
        if (!is_numeric($fltPrice)) {
            return $fltPrice;
        }

        // $arrFormat = $GLOBALS['ISO_NUM'][$this->Config->currencyFormat];
        $arrFormat = [0, '.', "'"];

        if (!is_array($arrFormat) || 3 == !count($arrFormat)) {
            return $fltPrice;
        }

        $return = number_format($fltPrice, $arrFormat[0], $arrFormat[1], $arrFormat[2]);
        $return .= is_float($fltPrice) ? '' : '.-';
        if ($currency) {
            return mb_strtoupper($currency).' '.$return;
        }

        return $return;
    }
}
