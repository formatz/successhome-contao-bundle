<?php

/**
 * Contao Open Source CMS.
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @author    Jonathan Sigg
 * @license   GNU/LGPL
 * @copyright format-z 2014
 */

/**
 * Namespace.
 */

namespace Formatz\SuccesshomeContaoBundle\Resources\contao\modules;

use Contao\BackendTemplate;
use Contao\Environment;
use Contao\Input;
use Contao\PageModel;
use Contao\SelectMenu;
use Contao\StringUtil;
use Contao\System;

class ModuleImmoSearch extends ModuleSuccessHome
{
    protected $strTemplate = 'immosearch';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        $request = System::getContainer()->get('request_stack')?->getCurrentRequest();

        if ($request && System::getContainer()->get('contao.routing.scope_matcher')?->isBackendRequest($request)) {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### '.mb_strtoupper($GLOBALS['TL_LANG']['FMD']['immo_search'][0]).' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile()
    {
        // check if form is send and save value of search in session
        $this->handleForm();

        self::loadLanguageFile('countries');

        $arrWidgets = [];
        $arrFields = StringUtil::deserialize($this->search_fields);
        $formData = $this->getImmoFormData();
        foreach ($arrFields as $fieldName) {
            $arrData = [];
            switch ($fieldName) {
                case 'city':
                    $strClass = $GLOBALS['TL_FFL']['text'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_place'];
                    $arrData['eval'] = ['placeholder' => $GLOBALS['TL_LANG']['MSC']['chosePlace']];
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;
                case 'radius':
                    $strClass = $GLOBALS['TL_FFL']['text'];
                    $arrData['eval'] = [];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_radius'];
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'types':
                    $strClass = $GLOBALS['TL_FFL']['select'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_types'];
                    $arrData['options'] = $this->getAvailableTypes();
                    $arrData['isAssociative'] = true;
                    $arrData['eval']['includeBlankOption'] = true;
                    $arrData['eval']['class'] = 'types2-multiselect';
                    $arrData['eval']['blankOptionLabel'] = $GLOBALS['TL_LANG']['MSC']['immo_search_types_all'];
                    $arrData['reference'] = $GLOBALS['TL_LANG']['MSC']['immo']['type2'];
                    $objWidget = new SelectMenu($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'country':
                    $strClass = $GLOBALS['TL_FFL']['select'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_country'];

                    // get country from webservice
                    $countries = $this->getImmoCountries($this->mandat);
                    $countries = array_combine($countries, $countries);
                    $postCountry = $formData[$fieldName];
                    $arrData['default'] = $postCountry ?? (in_array($this->defaultCountry, $countries, true) ? $this->defaultCountry : '');
                    $arrData['options'] = $countries;

                    $arrData['eval']['blankOptionLabel'] = $GLOBALS['TL_LANG']['MSC']['immo_search_country_all'];
                    $arrData['eval']['includeBlankOption'] = true;
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $arrData['default'], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'roomFrom':
                    $strClass = $GLOBALS['TL_FFL']['select'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_roomFrom'];

                    $rooms = range(1, 7.5, 0.5);
                    $roomsOptions = array_combine($rooms, $rooms);
                    $roomsOptions[8] = '8+';
                    $arrData['options'] = $roomsOptions;

                    $arrData['eval']['includeBlankOption'] = true;
                    $arrData['eval']['blankOptionLabel'] = $GLOBALS['TL_LANG']['MSC']['immo_search_room_all'];
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'roomTo':
                    $strClass = $GLOBALS['TL_FFL']['select'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_roomTo'];

                    $rooms = range(1, 7.5, 0.5);
                    $roomsOptions = array_combine($rooms, $rooms);
                    $roomsOptions['8'] = '8';
                    $arrData['options'] = $roomsOptions;

                    $arrData['eval']['includeBlankOption'] = true;
                    $arrData['eval']['blankOptionLabel'] = $GLOBALS['TL_LANG']['MSC']['immo_search_room_all'];
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'priceFrom':
                    $strClass = $GLOBALS['TL_FFL']['text'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_priceFrom'];
                    $arrData['eval']['class'] = 'price-format';
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'priceTo':
                    $strClass = $GLOBALS['TL_FFL']['text'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_priceTo'];
                    $arrData['eval']['class'] = 'price-format';
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'ref':
                    $strClass = $GLOBALS['TL_FFL']['text'];
                    $arrData['eval']['class'] = '';
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_ref'];
                    $objWidget = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $formData[$fieldName], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;

                case 'homecreationType':
                    $strClass = $GLOBALS['TL_FFL']['select'];
                    $arrData['eval']['includeBlankOption'] = true;
                    $arrData['eval']['class'] = 'types2-multiselect';
                    $arrData['options'] = ['projet', 'realisation', 'modele'];
                    $arrData['reference'] = &$GLOBALS['TL_LANG']['MSC']['homecreationType_options'];
                    $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_search_types'];
                    $objWidget = new SelectMenu($strClass::getAttributesFromDca($arrData, $fieldName, $formData['homecreationtype'], '', '', $this));
                    $arrWidgets[$fieldName] = $objWidget->parse();
                    break;
            }
        }

        if (!empty($arrWidgets)) {
            $this->Template->cityLat = $formData['lat'] ?? '';
            $this->Template->cityLng = $formData['lng'] ?? '';
            $this->Template->typesOptions = $this->getAvailableTypes();
            $this->Template->previousSearch = $formData['city'];
            $this->Template->selectedTypes = System::getContainer()->get('request_stack')?->getCurrentRequest()->getSession()->get(self::SESSION_SEARCH_TYPES) ?: [];
            $this->Template->enableTypes = in_array('types', $arrFields);
            $this->Template->arrWidgets = $arrWidgets;
            $this->Template->formId = 'immo_search';
            $this->Template->slabel = StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['search']);
            $this->Template->moreLabel = $GLOBALS['TL_LANG']['MSC']['moreLabel'];
            $this->Template->lessLabel = $GLOBALS['TL_LANG']['MSC']['lessLabel'];
            $this->Template->allLabel = $GLOBALS['TL_LANG']['MSC']['allLabel'];
            $this->Template->selectedLabel = $GLOBALS['TL_LANG']['MSC']['selectedLabel'];
            $this->Template->categoryLabel = $GLOBALS['TL_LANG']['MSC']['categoryLabel'];
            $this->Template->action = Environment::get('indexFreeRequest');
            $this->Template->enctype = 'application/x-www-form-urlencoded';
            $this->Template->csrfToken = System::getContainer()->get('contao.csrf.token_manager')?->getDefaultTokenValue();
        }
    }

    protected function handleForm()
    {
        // Save value of serach in session
        if ('immo_search' === Input::post('FORM_SUBMIT')) {
            $arrFields = StringUtil::deserialize($this->search_fields);
            $arrFields[] = 'cityLat';
            $arrFields[] = 'cityLng';
            $formData = $this
                ->resetImmoFormData()
                ->getImmoFormData()
            ;
            foreach ($arrFields as $field) {
                if (($value = Input::findPost($field)) !== null) {
                    $formData[$field] = $value;
                }
            }

            if (
                (null !== $formData['radius'] && $formData['radius'] <= 0)
                || (null === $formData['radius'] && !empty($formData['city']))
            ) {
                $formData['radius'] = self::DEFAULT_RADIUS;
            }

            $this->setImmoFormData($formData);

            if ($this->jumpTo) {
                $objPageList = PageModel::findPublishedById($this->jumpTo);
                // Contao 5
                if (System::getContainer()->has('contao.routing.content_url_generator')) {
                    $urlGenerator = System::getContainer()->get('contao.routing.content_url_generator');
                    self::redirect($urlGenerator->generate($objPageList->current()));
                }
                // Contao 4
                else {
                    self::redirect($this->generateFrontendUrl($objPageList->row()));
                }
            } else {
                self::redirect($this->addToUrl('p='));
            }
        }
    }
}
