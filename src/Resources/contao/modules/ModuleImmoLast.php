<?php

namespace Formatz\SuccesshomeContaoBundle\Resources\contao\modules;

use Contao\BackendTemplate;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;

class ModuleImmoLast extends ModuleSuccessHome
{
    protected $strTemplate = 'immolast';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        $request = System::getContainer()->get('request_stack')->getCurrentRequest();

        if ($request && System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest($request)) {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### '.mb_strtoupper($GLOBALS['TL_LANG']['FMD']['immo_list'][0]).' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile()
    {
        $objPage = PageModel::findPublishedById($this->jumpTo);

        if (null === $objPage) {
            $this->Template->immos = [];

            return 'Please select a reader page';
        }

        // array of options for search
        $options = [];

        if ('both' !== $this->mandat) {
            $options['mandat'] = $this->mandat;
        }

        $number = $this->number > 0 ? $this->number : 1;

        if ($this->displayEndSoldProperties && $number < 99) {
            // ignore sold because we want at the end of list but cannot know how many sold properties we have
            // TODO: sort by wording, with "sold" at the end then like today - or get all immo object then sort the all array
            $options['ignoreSold'] = 1;
        }

        $arrData = $this->getImmoList($options, 1, $number);
        // $data = array_shift(array_slice($arrData, 0, 1)); // get the only one

        if (!empty($arrData)) {
            foreach ($arrData as $index => $data) {
                $arrData[$index]['description'] = strlen($arrData[$index]['description']) > 300 ? mb_substr($arrData[$index]['description'], 0, 300).'...' : $arrData[$index]['description'];
                $arrData[$index]['name'] = strlen($data['name']) > 80 ? mb_substr($data['name'], 0, 80).'...' : $data['name'];
                $slugImmo = explode('/', $data['slug']);
                $param = '/immo/'.end($slugImmo);

                // Contao 5
                if (System::getContainer()->has('contao.routing.content_url_generator')) {
                    $urlGenerator = System::getContainer()->get('contao.routing.content_url_generator');
                    $link = $urlGenerator->generate($objPage->current()).$param;
                }
                // Contao 4
                else {
                    $link = $this->generateFrontendUrl($objPage->row(), $param);
                }
                $arrData[$index]['href'] = StringUtil::ampersand($link);
            }
        }

        // immos
        $this->Template->immos = $arrData;
        $this->Template->inactiveSoldProperties = $this->inactiveSoldProperties;
        // label for view
        $this->Template->immo_city_label = $GLOBALS['TL_LANG']['MSC']['immo_city_label'];
        $this->Template->immo_state_label = $GLOBALS['TL_LANG']['MSC']['immo_state_label'];
        $this->Template->immo_room_label = $GLOBALS['TL_LANG']['MSC']['immo_room_label'];
        $this->Template->immo_living_space_label = $GLOBALS['TL_LANG']['MSC']['immo_living_space_label'];
        $this->Template->immo_bedroom_label = $GLOBALS['TL_LANG']['MSC']['immo_bedroom_label'];
        $this->Template->read_more_label = $GLOBALS['TL_LANG']['MSC']['read_more'];
        $this->Template->on_demand_label = $GLOBALS['TL_LANG']['MSC']['immo']['priceOnDemand']['_label'];
    }
}
