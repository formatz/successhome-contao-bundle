<?php

namespace Formatz\SuccesshomeContaoBundle\Resources\contao\modules;

use Contao\BackendTemplate;
use Contao\Environment;
use Contao\Input;
use Contao\PageError404;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Formatz\SuccesshomeContaoBundle\Resources\contao\classes\ImmoContact;

class ModuleImmoReader extends ModuleSuccessHome
{
    protected $strTemplate = 'immoreader';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        $request = System::getContainer()->get('request_stack')->getCurrentRequest();

        if ($request && System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest($request)) {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### '.mb_strtoupper($GLOBALS['TL_LANG']['FMD']['immo_reader'][0]).' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Do not index or cache the page if no immo item has been specified
        if (!Input::get('immo')) {
            /* @var PageModel $objPage */
            global $objPage;

            $objPage->noSearch = 1;
            $objPage->cache = 0;

            return '';
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile()
    {
        /* @var PageModel $objPage */
        global $objPage;

        $objSession = System::getContainer()->get('request_stack')->getCurrentRequest()->getSession();
        if (!isset($objSession->get(self::ALL_IMMO_INDEX)[Input::get('immo')])) {
            $this->getImmoList([], 1, 0);
        }

        $arrData = $this->getImmo(Input::get('immo'));

        if (empty($arrData)) {
            /** @var PageError404 $objHandler */
            $objHandler = new $GLOBALS['TL_PTY']['error_404']();
            $objHandler->generate($objPage->id);
        }

        // Verify if sold item is allowed
        if ('1' === $this->inactiveSoldProperties && 'sold' === $arrData['wording']) {
            /** @var PageError404 $objHandler */
            $objHandler = new $GLOBALS['TL_PTY']['error_404']();
            $objHandler->generate($objPage->id);
        }

        // Translations and values management
        $i = 0;
        foreach ($arrData['specs'] as $spec => $value) {
            // if no value, continue. If price, handle later
            if (empty($value) && 'price' !== $spec) {
                unset($arrData['specs'][$spec]);
                continue;
            }

            $arrData['specs'][$spec] = [
                'label' => $GLOBALS['TL_LANG']['MSC']['immo'][$spec]['_label'],
                'index' => ++$i,
                'value' => $value,
            ];

            if ('price' === $spec && 'rent' === $arrData['mandat'] && isset($GLOBALS['TL_LANG']['MSC']['immo'][$spec]['_label_rent'])) {
                // label en "location" au lieu de prix si location et que la traduction existe
                $arrData['specs'][$spec]['label'] = $GLOBALS['TL_LANG']['MSC']['immo'][$spec]['_label_rent'];
            }

            if ('park_place_int_price' === $spec) {
                $arrData['specs'][$spec]['value'] = $arrData['park_place_int_exclude'] ? $value.$GLOBALS['TL_LANG']['MSC']['immo']['park_price']['exclude'] : $value.$GLOBALS['TL_LANG']['MSC']['immo']['park_price']['include'];
            } elseif ('park_place_ext_price' === $spec) {
                $arrData['specs'][$spec]['value'] = $arrData['park_place_ext_exclude'] ? $value.$GLOBALS['TL_LANG']['MSC']['immo']['park_price']['exclude'] : $value.$GLOBALS['TL_LANG']['MSC']['immo']['park_price']['include'];
            } elseif ('park_place_garage_price' === $spec) {
                $arrData['specs'][$spec]['value'] = $arrData['park_place_garage_exclude'] ? $value.$GLOBALS['TL_LANG']['MSC']['immo']['park_price']['exclude'] : $value.$GLOBALS['TL_LANG']['MSC']['immo']['park_price']['include'];
            } elseif (is_string($value) && !in_array($spec, ['room', 'price', 'availability', 'expense', 'rental_gross', 'living_space', 'commercial_space', 'ground_space', 'garden_space', 'gross_yield', 'total_area', 'ground_surface_to_build', 'living_surface_to_build']) && (string) (int) $value != $value) {
                $arrData['specs'][$spec]['value'] = ucfirst($GLOBALS['TL_LANG']['MSC']['immo'][$spec][$value]);
            } elseif ('price' === $spec && !$value) {
                $arrData['specs'][$spec]['value'] = $GLOBALS['TL_LANG']['MSC']['immo']['priceOnDemand']['_label'];
            } elseif (is_array($value)) {
                $arrValue = $value;
                foreach ($arrValue as $index => $valueKey) {
                    $arrValue[$index] = $GLOBALS['TL_LANG']['MSC']['immo'][$spec][$valueKey];
                }
                $arrData['specs'][$spec]['value'] = ucfirst(implode(', ', $arrValue));
            }
        }

        // handle country
        $arrData['country'] = System::getContainer()->get('contao.intl.countries')?->getCountries()[$arrData['country']];

        $this->Template->immo = $arrData;

        $immoContact = new ImmoContact();
        $immoContact->setPhone(Input::post('phone'));
        $immoContact->setLastname(Input::post('lastname'));
        $immoContact->setFistname(Input::post('firstname'));
        $immoContact->setEmail(Input::post('email'));
        $immoContact->setDemand(Input::post('demand'));

        $this->saveContactInfoInSession($immoContact);

        // form immo contact
        $fieldName = 'firstname';
        $strClass = $GLOBALS['TL_FFL']['text'];
        $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_contact_firstname'];
        $arrData['eval'] = [
            'mandatory' => true];
        $objWidgets[$fieldName] = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $objSession->get(self::SESSION_CONTACT_FIRSTNAME), '', '', $this));
        $arrWidgets[$fieldName] = $objWidgets[$fieldName]->parse();

        $fieldName = 'lastname';
        $strClass = $GLOBALS['TL_FFL']['text'];
        $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_contact_lastname'];
        $arrData['eval'] = [
            'mandatory' => true];
        $objWidgets[$fieldName] = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $objSession->get(self::SESSION_CONTACT_LASTNAME), '', '', $this));
        $arrWidgets[$fieldName] = $objWidgets[$fieldName]->parse();

        $fieldName = 'email';
        $strClass = $GLOBALS['TL_FFL']['text'];
        $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_contact_email'];
        $arrData['eval'] = ['rgxp' => 'emails',
            'mandatory' => true];
        $objWidgets[$fieldName] = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $objSession->get(self::SESSION_CONTACT_EMAIL), '', '', $this));
        $arrWidgets[$fieldName] = $objWidgets[$fieldName]->parse();

        $fieldName = 'phone';
        $strClass = $GLOBALS['TL_FFL']['text'];
        $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_contact_phone'];
        $arrData['eval'] = [
            'mandatory' => true];
        $objWidgets[$fieldName] = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $objSession->get(self::SESSION_CONTACT_PHONE), '', '', $this));
        $arrWidgets[$fieldName] = $objWidgets[$fieldName]->parse();

        $fieldName = 'demand';
        $strClass = $GLOBALS['TL_FFL']['textarea'];
        $arrData['label'] = $GLOBALS['TL_LANG']['MSC']['immo_contact_demand'];
        $arrData['eval'] = [
            'mandatory' => false,
        ];
        $objWidgets[$fieldName] = new $strClass($strClass::getAttributesFromDca($arrData, $fieldName, $objSession->get(self::SESSION_CONTACT_DEMAND), '', '', $this));
        $arrWidgets[$fieldName] = $objWidgets[$fieldName]->parse();

        // handle form
        if ('immo_contact' === Input::post('FORM_SUBMIT')) {
            $doNotSubmit = false;
            foreach ($objWidgets as $name => $objWidget) {
                $objWidget->validate();

                if ($objWidget->hasErrors()) {
                    $doNotSubmit = true;
                    $arrWidgets[$name] = $objWidget->parse();
                }
            }

            if (!$doNotSubmit) {
                $response = $this->postNewContact(Input::get('immo'), $immoContact);
                $this->Template->sendSuccess = $response;
            }
        }

        $arrConfigurationFlex = [
            'alias' => 'slider-immo',
            'slideshowSpeed' => 3000,
            'animationSpeed' => 600,
            'animation' => 'fade',
            'direction' => 'horizontal',
            'css_theme' => '',
            'cssSRC' => '',
            'jqeasing' => 0,
            'controlNav' => 1,
            'directionNav' => 1,
            'randomize' => '',
            'pauseOnHover' => '',
            'imgLinks' => '',
            'imgDesc' => '',
            'carousel' => 1,
            'itemWidth' => 80,
            'itemMargin' => 9,
        ];

        $this->Template->ifPictures = count($arrData['images']);
        $this->Template->configuration = [$arrConfigurationFlex];
        $this->Template->widgets = $arrWidgets;
        $this->Template->formId = 'immo_contact';
        $this->Template->slabel = StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['send']);
        $this->Template->action = Environment::get('indexFreeRequest');
        $this->Template->enctype = 'application/x-www-form-urlencoded';
        $this->Template->csrfToken = System::getContainer()->get('contao.csrf.token_manager')?->getDefaultTokenValue();
    }
}
