<?php

use Contao\Backend;
use Contao\System;

$GLOBALS['TL_DCA']['tl_module']['palettes']['immo_list'] = '{title_legend},name,headline,type;{config_legend},perPage,mandat,number,wording,types,inactiveSoldProperties,displayEndSoldProperties;{redirect_legend},jumpTo,jumpToPrestige,jumpToBusiness;{template_legend},customTpl;';
$GLOBALS['TL_DCA']['tl_module']['palettes']['immo_last'] = '{title_legend},name,headline,type;{config_legend},mandat;number,inactiveSoldProperties,displayEndSoldProperties;{redirect_legend},jumpTo;{template_legend},customTpl;';
$GLOBALS['TL_DCA']['tl_module']['palettes']['immo_reader'] = '{title_legend},name,headline,type,inactiveSoldProperties;{template_legend},customTpl;';
$GLOBALS['TL_DCA']['tl_module']['palettes']['immo_search'] = '{title_legend},name,headline,type;search_fields;jumpTo;wording;mandat;defaultCountry;{template_legend},customTpl;';

$GLOBALS['TL_DCA']['tl_module']['fields']['number'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['number'],
    'inputType' => 'text',
    'eval' => ['rgxp' => 'digit', 'nospace' => true, 'tl_class' => 'w50'],
    'sql' => "int(10) NOT NULL default '0'",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['search_fields'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['search_fields'],
    'inputType' => 'checkbox',
    'default' => 'immo',
    'options_callback' => ['tl_module_successhome', 'getSearchFields'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_module']['fields']['wording'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['wordingList'],
    'inputType' => 'select',
    'options_callback' => ['tl_module_successhome', 'getWordingList'],
    'eval' => ['mandatory' => true, 'tl_class' => 'w50'],
    'sql' => "varchar(64) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['defaultCountry'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['countryList'],
    'inputType' => 'select',
    'options' => System::getContainer()->get('contao.intl.countries')->getCountries(),
    'eval' => ['mandatory' => true, 'chosen' => true],
    'sql' => "varchar(2) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['mandat'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['mandatList'],
    'inputType' => 'select',
    'default' => 'both',
    'eval' => ['mandatory' => true, 'tl_class' => 'w50'],
    'options_callback' => ['tl_module_successhome', 'getMandatList'],
    'sql' => "varchar(64) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['types'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['typesList'],
    'inputType' => 'select',
    'default' => 'all',
    'eval' => ['mandatory' => true, 'tl_class' => 'w50'],
    'options_callback' => ['tl_module_successhome', 'getTypesList'],
    'sql' => "varchar(64) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['inactiveSoldProperties'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['inactiveSoldProperties'],
    'inputType' => 'checkbox',
    'eval' => ['mandatory' => false, 'tl_class' => 'w50 clr'],
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['displayEndSoldProperties'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['displayEndSoldProperties'],
    'inputType' => 'checkbox',
    'eval' => ['mandatory' => false, 'tl_class' => 'w50 clr'],
    'sql' => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToPrestige'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['jumpToPrestige'],
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'eager'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToBusiness'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['jumpToPrestige'],
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'eager'],
];

class tl_module_successhome extends Backend
{
    /**
     * Get all fields for search module and return them as array.
     *
     * @return array
     */
    public function getSearchFields()
    {
        $arrFields = [
            'country' => $GLOBALS['TL_LANG']['MSC']['immo_search_country'],
            'types' => $GLOBALS['TL_LANG']['MSC']['immo_search_types'],
            'city' => $GLOBALS['TL_LANG']['MSC']['immo_search_place'],
            'radius' => $GLOBALS['TL_LANG']['MSC']['immo_search_radius'],
            'roomFrom' => $GLOBALS['TL_LANG']['MSC']['immo_search_roomFrom'],
            'roomTo' => $GLOBALS['TL_LANG']['MSC']['immo_search_roomTo'],
            'priceFrom' => $GLOBALS['TL_LANG']['MSC']['immo_search_priceFrom'],
            'priceTo' => $GLOBALS['TL_LANG']['MSC']['immo_search_priceTo'],
            'ref' => $GLOBALS['TL_LANG']['MSC']['immo_search_ref'],
            'search' => $GLOBALS['TL_LANG']['MSC']['keywords'],
            'homecreationType' => $GLOBALS['TL_LANG']['MSC']['immo_search_homecreationType'],
        ];

        return $arrFields;
    }

    /**
     * Get WordingList.
     *
     * @return array
     */
    public function getWordingList()
    {
        $arrWording = [
            'immo' => $GLOBALS['TL_LANG']['MSC']['immo_wording_immo'],
            'prestige' => $GLOBALS['TL_LANG']['MSC']['immo_wording_prestige'],
            'yieldImmo' => $GLOBALS['TL_LANG']['MSC']['immo_wording_yieldImmo'],
        ];

        return $arrWording;
    }

    /**
     * Get MandatList.
     *
     * @return array
     */
    public function getMandatList()
    {
        $arrMandats = [
            'both' => $GLOBALS['TL_LANG']['MSC']['immo_mandat_both'],
            'rent' => $GLOBALS['TL_LANG']['MSC']['immo_mandat_rent'],
            'sale' => $GLOBALS['TL_LANG']['MSC']['immo_mandat_sale'],
        ];

        return $arrMandats;
    }

    /**
     * Get MandatList.
     *
     * @return array
     */
    public function getTypesList()
    {
        $arrTypes = [
            'all' => $GLOBALS['TL_LANG']['MSC']['immo_mandat_both'],
            'flat' => $GLOBALS['TL_LANG']['MSC']['immo_types']['flat'],
            'house' => $GLOBALS['TL_LANG']['MSC']['immo_types']['house'],
            'site' => $GLOBALS['TL_LANG']['MSC']['immo_types']['site'],
            'building' => $GLOBALS['TL_LANG']['MSC']['immo_types']['building'],
            'commercial' => $GLOBALS['TL_LANG']['MSC']['immo_types']['commercial'],
            'parking' => $GLOBALS['TL_LANG']['MSC']['immo_types']['parking'],
        ];

        return $arrTypes;
    }
}
