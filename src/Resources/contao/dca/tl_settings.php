<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_settings']['fields']['successhome_url'] = [
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'decodeEntities' => true, 'tl_class' => 'w50'],
];

$GLOBALS['TL_DCA']['tl_settings']['fields']['successhome_apikey'] = [
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'decodeEntities' => true, 'tl_class' => 'w50'],
];

PaletteManipulator::create()
    ->addLegend('successhome_legend', 'global_legend')
    ->addField('successhome_url', 'successhome_legend', PaletteManipulator::POSITION_APPEND)
    ->addField('successhome_apikey', 'successhome_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_settings');
