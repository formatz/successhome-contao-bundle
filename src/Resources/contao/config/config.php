<?php

use Formatz\SuccesshomeContaoBundle\Resources\contao\modules\ModuleImmoLast;
use Formatz\SuccesshomeContaoBundle\Resources\contao\modules\ModuleImmoList;
use Formatz\SuccesshomeContaoBundle\Resources\contao\modules\ModuleImmoReader;
use Formatz\SuccesshomeContaoBundle\Resources\contao\modules\ModuleImmoSearch;

$GLOBALS['FE_MOD']['successhome'] = [
    'immo_list' => ModuleImmoList::class,
    'immo_reader' => ModuleImmoReader::class,
    'immo_search' => ModuleImmoSearch::class,
    'immo_last' => ModuleImmoLast::class,
];
